(ns ga-clojure.core
  (:gen-class)
  (:import (javax.swing JFrame JPanel JFileChooser)
           (java.awt Color GridLayout)
           (java.awt.image BufferedImage PixelGrabber)
           (java.io File)
           (javax.swing.filechooser FileNameExtensionFilter)
           (javax.imageio ImageIO)))

;----------------------------------------------------------------------------------------
;Helpers functions

"Define the max number of circles"
(def max-circles 200)

"Return a random double between -1 and 1"
(defn random-double []
  (- (* 2 (rand)) 1))

"Normalize a value with respect to the RGB scale (0-255)"
(defn norm-color [color-channel]
  (max (min color-channel 255) 0))

"Remove the @item from the @list"
(defn remove-item [list item]
  (concat (take item list) (drop (inc item) list)))

"Replace the @item with the @replace in list @l"
(defn replace-item [l item replace]
  (concat (take item l) (list replace) (drop (inc item) l)))

"Return an array of pixel of @image"
(defn pixel-grabber [image]
  (let [width (.getWidth image)
        height (.getHeight image)
        pixels (make-array (. Integer TYPE) (* width height))]
    (doto (new PixelGrabber image 0 0 width height pixels 0 width)
      (.grabPixels))
    pixels))

;----------------------------------------------------------------------------------------
;Drawing Elements (Point, Color, Circle)

"Define a Circle Point with coordinates @x and @y and a ray @r"
(defn point [x y radius]
  {:type :Point :x x :y y :radius radius})

"Return a random Circle Point in @image"
(defn random-point [image]
  (point (rand-int (.getWidth image))
         (rand-int (.getHeight image))
         (rand-int (.getHeight image))))

"Define a Color with RGB (@red, @green and @blue) and @alpha channels"
(defn color [red green blue alpha]
  {:type :Color :red red :blue blue :green green :alpha alpha})

"Define a Circle with @point coordinate and @color"
(defn circle [color point]
  {:type :Circle :color color :point point})

"Draw a Circle"
(defn paint-circle [graphics {color :color point :point}]
  (.setColor graphics (new Color (:red color) (:blue color) (:green color) (:alpha color)))
  (.fillOval graphics (:x point) (:y point) (:radius point) (:radius point))
  nil)

"Apply eval function"
(defn apply-eval [individual gen-image]
  (try
    (apply (eval (:code individual)) [(.createGraphics gen-image)])
    (catch IndexOutOfBoundsException e (str "cuaght exception: " (.getMessage e)))))

;----------------------------------------------------------------------------------------
;Utility functions for GA

"Define an Outcome with an associated @code, @fitness and @image"
(defn outcome [code fitness image]
  {:type :Outcome :code code :fitness fitness :image image})

"Get the first two elements of the code of @outc"
(defn outcome-header [outc]
  (take 2 (:code outc)))

"Get the elements of code of @outc except for the header"
(defn outcome-noheader [outc]
  (drop (count (outcome-header outc)) (:code outc)))

"Generate an initial outcome"
(def initial-outcome (outcome '(fn [graphics]) nil nil))

;----------------------------------------------------------------------------------------
;Implementation fitness function

"Calculate the distance (with Least Mean Square method) between two pixel sequence, @seq-pixa and @seq-pixb"
(defn least-mean-square [seq-pixa seq-pixb]
  (letfn [ (lms [a b]
             (let [source-color (new Color a)
                   generate-color (new Color b)
                   delta-red (- (.getRed source-color) (.getRed generate-color))
                   delta-green (- (.getGreen source-color) (.getGreen generate-color))
                   delta-blue (- (.getBlue source-color) (.getBlue generate-color))]
               (+ (* delta-red delta-red) (* delta-green delta-green) (* delta-blue delta-blue))))]
    (reduce + (map lms seq-pixa seq-pixb))))

"Calculate the Fitness function in term of distance between the source image and the generated image"
(defn fitness-function [individual image]
  (if (:fitness individual)
    individual
    (let [generated-image (new BufferedImage (.getWidth image) (.getHeight image) BufferedImage/TYPE_INT_ARGB)
          source-pixel (pixel-grabber image)]
      (apply-eval individual generated-image)
      (let [generated-pixel (pixel-grabber generated-image)
            lms (least-mean-square generated-pixel source-pixel)]
        (assoc individual :fitness lms :image generated-image)))))

"Select the @n elements from @individuals with the best fitness respect to @image"
(defn selection [individuals n image]
  (take n (sort-by :fitness (pmap #(fitness-function % image) individuals))))

;----------------------------------------------------------------------------------------
;Mutation sections: Implementation of various mutation functions

"Define a mulimethod for mutation"
(defmulti mutation :type)

"Apply a mutation to a Circle Point type"
(defmethod mutation :Point [{x :x y :y radius :radius :as p} image]
  (let [deltax (int (* x (random-double)))
        deltay (int (* y (random-double)))
        deltaradius (int (* radius (random-double)))]
    (assoc p :x (max (min (- (:x p) deltax) (.getWidth image)) 0)
             :y (max (min (- (:y p) deltay) (.getHeight image)) 0)
             :r (max (min (- (:radius p) deltaradius) (.getHeight image)) 0)
             )
    ))

"Mutate the point of a Circle"
(defn mutation-point [p point image]
  (assoc p :point (mutation point image)))

"Apply mutation to a Color type"
(defmethod mutation :Color [color]
  (let [deltared (int (* (:red color) (random-double)))
        deltagreen (int (* (:green color) (random-double)))
        deltablue (int (* (:blue color) (random-double)))
        deltaalpha (int (* (:alpha color (random-double))))]
    (assoc color :red (norm-color (-(:red color) deltared))
                 :green (norm-color (-(:green color) deltagreen))
                 :blue (norm-color (-(:blue color) deltablue))
                 :alpha (norm-color (-(:alpha color) deltaalpha)))))

"Mutate the color of a Circle"
(defn mutation-color [p]
  (assoc p :color (mutation (:color p))))

"Apply mutation to a Circle"
(defmethod mutation :Circle [{point :point, :as p} image]
  (let [randchoice (rand-int 2)]
    (cond
      (= 0 randchoice) (mutation-point p point image)
      (= 1 randchoice) (mutation-color p))))

"Mutate a Circle"
(defn mutation-circle [p image]
  (let [circles (outcome-noheader p)
        i (rand-int (count circles))
        target (nth circles i)]
    (assoc p :code (concat (outcome-header p) (replace-item circles i (list (nth target 0) (nth target 1) (mutation (nth target 2) image))))
             :fitness nil :image nil)))

"Add a Circle"
(defn add-circle [p image]
  (assoc p :code
           (concat (:code p)
                   [(list 'ga-clojure.core/paint-circle
                          (first (nth (:code initial-outcome) 1))
                          (circle
                            (color (rand-int 255)
                                   (rand-int 255)
                                   (rand-int 255)
                                   (rand-int 255))
                            (random-point image)))])
           :fitness nil :image nil))

"Remove a Circle"
(defn remove-circle [p]
  (let [num-circles (count (outcome-noheader p))
        n (rand-int num-circles)]
    (assoc p :code (concat (outcome-header p)
                           (remove-item (outcome-noheader p) n))
             :fitness nil :image nil)))

"Apply mutation to an Outcome"
(defmethod mutation :Outcome [p image]
  (let [num-circles (count (outcome-noheader p))
        randchoice (cond
                     (empty? (outcome-noheader p)) 4
                     (>= num-circles max-circles) (rand-int 4)
                     :else (rand-int 5))]
    (cond
      (> 3 randchoice) (mutation-circle p image)
      (= 3 randchoice) (remove-circle p)
      (= 4 randchoice) (add-circle p image))))

;----------------------------------------------------------------------------------------
;Evolution Function

"Evolve a population"
(defn evolution [settings image]
  (loop[i 0 population(list initial-outcome)]
    (println "Generation " i)
    (let [best-fit (selection population 1 image)
          offsprings (map #(mutation % image) best-fit)]
      ;(println best-fit)
      ((:new-generation-callback settings (fn [a b])) i best-fit)
      (when-not (= (first population) (first best-fit))
        ((:new-bestfit-callback settings (fn [a b])) i best-fit))
      (recur (inc i) (concat best-fit offsprings)))))

;----------------------------------------------------------------------------------------

"Load the source image from a File Chooser"
(defn load-image []
  (let [file-chooser (new JFileChooser)
        filter (new FileNameExtensionFilter "Image File" (into-array ["jpg" "jpeg"]))]
    (doto file-chooser
      (.setCurrentDirectory (new File "."))
      (.setFileFilter filter)
      (.addChoosableFileFilter filter)
      (.showOpenDialog nil))
    (ImageIO/read (.getSelectedFile file-chooser))))

;----------------------------------------------------------------------------------------
"User GUI"
(defn gui [image]
  (let [jframe (new JFrame "Genetic Algorithm")
        best-fit (atom (list initial-outcome))
        settings {:new-bestfit-callback (fn [i f] (swap! best-fit (fn [o n] n) f) (.repaint jframe))}]
    (doto jframe
      (.setSize (+ 10 (* 2 (.getWidth image))) (+ 50 (.getHeight image)))
      (.setLocationRelativeTo nil)
      (.setLayout (new GridLayout 1 2))
      (.add (proxy [JPanel] []
              (paint [g]
                (doto g
                  (.setColor Color/WHITE)
                  (.fillRect 0 0 (.getWidth image) (.getHeight image))
                  (.drawImage image nil 0 0)))))
      (.add (proxy [JPanel] []
              (paint [g]
                (doto g
                  (.setColor Color/white)
                  (.fillRect 0 0 (.getWidth image) (.getHeight image))
                  (.drawImage (:image (first @best-fit)) nil 0 0)))))
      (.setDefaultCloseOperation JFrame/EXIT_ON_CLOSE)
      (.setVisible true))
    (evolution settings image)))

;----------------------------------------------------------------------------------------

"Main Method"
(defn -main []
  (let [img (load-image)]
    (gui img)))